import './App.css'
import React from 'react'
import Book from "./Book";
import PropTypes from "prop-types";

class BookGrid extends React.Component {

  static propTypes = {
    books: PropTypes.array.isRequired,
    onUpdateBook: PropTypes.func.isRequired
  };

  render() {
    const {books, onUpdateBook} = this.props;
    return (
      <ol className="books-grid">
        { books.map(book => (
          <li key={book.id}>
            <Book book={book} onUpdateBook={(book, shelf) => onUpdateBook(book, shelf)}/>
          </li>
        ))}
      </ol>
    )
  }
}

export default BookGrid
