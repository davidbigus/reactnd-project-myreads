import './App.css'
import React from 'react'
import * as BooksAPI from './BooksAPI'
import SearchBar from "./SearchBar";
import BookGrid from "./BookGrid";
import PropTypes from "prop-types";

class BooksApp extends React.Component {

  static propTypes = {
    books: PropTypes.array.isRequired,
    onUpdateBook: PropTypes.func.isRequired,
  };

  state = {
    books: [],
    error: false,
    emptyQuery: true,
  };

  searchBooks(query) {
    if (query.length) {
      BooksAPI.search(query).then(response => {
        if (response !== undefined && !response.error) {
          for (let item of response) {
            const index = this.props.books.findIndex(book => book.id === item.id);
            item.shelf = (index >= 0) ? this.props.books[index].shelf : 'none';
          }
          this.setState({
            books: response,
            error: false,
            emptyQuery: false,
          });
        } else this.setState({error: true, emptyQuery: false});
      })
    } else {
      this.setState({emptyQuery: true, books: []});
    }
  };

  render() {
    return (
      <div className="search-books">
        <SearchBar onSearch={(query) => this.searchBooks(query)} />
        <div className="search-books-results">
        {(this.state.books.length && !this.state.error) ?
          <BookGrid books={this.state.books} onUpdateBook={(book, shelf) => this.props.onUpdateBook(book, shelf)} />
          : (!this.state.emptyQuery || this.state.error) ? <p>No books found.</p> : ''}
        </div>
      </div>
    )
  }
}

export default BooksApp