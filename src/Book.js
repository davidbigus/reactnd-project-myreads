import React from 'react'
import './App.css'
import BookShelfChanger from "./BookShelfChanger";
import PropTypes from 'prop-types';

class Book extends React.Component {

  static propTypes = {
    book: PropTypes.object.isRequired,
    onUpdateBook: PropTypes.func.isRequired
  };

  render() {
    const {book, onUpdateBook} = this.props;

    return (
      <div className="book">
        <div className="book-top">
          {book.imageLinks && <div
            className="book-cover"
            style={{backgroundImage: `url(${book.imageLinks.thumbnail})`}}/>}
          <BookShelfChanger
            onUpdate={(shelf) => onUpdateBook(book, shelf)}
            current={book.shelf}
          />
        </div>
        <div className="book-title">{book.title}</div>
        <div className="book-authors">
          {book.authors && book.authors.join(', ')}
        </div>
      </div>
    )
  }
}

export default Book
