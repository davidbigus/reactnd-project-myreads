import './App.css'
import React from 'react'
import BookGrid from "./BookGrid";
import PropTypes from "prop-types";

class Shelf extends React.Component {

  static propTypes = {
    name: PropTypes.string.isRequired,
    books: PropTypes.array.isRequired,
    onUpdateBook: PropTypes.func.isRequired,
  };

  render() {
    const {name, books, onUpdateBook} = this.props;
    return (
      <div className="bookshelf">
        <h2 className="bookshelf-title">{name}</h2>
        <div className="bookshelf-books">
          <BookGrid books={books} onUpdateBook={(book, shelf) => onUpdateBook(book, shelf)}/>
        </div>
      </div>
    )
  }
}

export default Shelf
