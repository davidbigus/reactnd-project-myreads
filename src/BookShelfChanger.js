import './App.css'
import React from 'react'
import {shelves} from "./App";
import PropTypes from "prop-types";

class BookShelfChanger extends React.Component {

  static propTypes = {
    current: PropTypes.string.isRequired,
    onUpdate: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onUpdate(event.target.value);
  }

  render() {
    return (
      <div className="book-shelf-changer">
        <select value={this.props.current} onChange={this.handleChange}>
          <option value="move" disabled>Move to...</option>
          {shelves.map(shelf => (
            <option key={shelf.id} value={shelf.id}>{shelf.name}</option>
          ))}
          <option value="none">None</option>
        </select>
      </div>
    )
  }
}

export default BookShelfChanger
