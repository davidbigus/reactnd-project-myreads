import './App.css'
import React from 'react'
import {Link} from 'react-router-dom'
import Shelf from "./Shelf";
import {shelves} from "./App";
import PropTypes from "prop-types";

class OverviewPage extends React.Component {

  static propTypes = {
    books: PropTypes.array.isRequired,
    onUpdateBook: PropTypes.func.isRequired
  };

  render() {
    const {books, onUpdateBook} = this.props;
    return (
      <div className="list-books">
        <div className="list-books-title">
          <h1>MyReads</h1>
        </div>
        <div className="list-books-content">
          <div>
            { shelves.map( shelf => (
              <Shelf
                key={shelf.id}
                name={shelf.name}
                books={books.filter(book => book.shelf === shelf.id) }
                onUpdateBook={(book, shelf) => onUpdateBook(book, shelf)}
              />
            ))}
          </div>
        </div>
        <div className="open-search">
          <Link to='/search'>Add a book</Link>
        </div>
      </div>
    )
  }
}

export default OverviewPage
