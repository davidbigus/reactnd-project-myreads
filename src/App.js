import './App.css'
import React from 'react'
import * as BooksAPI from './BooksAPI'
import {Route, Switch} from 'react-router-dom'
import OverviewPage from "./OverviewPage";
import SearchPage from "./SearchPage";

export const shelves = [
  {name: 'Currently Reading', id: 'currentlyReading'},
  {name: 'Want to Read', id: 'wantToRead'},
  {name: 'Read', id: 'read'},
];

class BooksApp extends React.Component {

  state = {
    books: [],
  };

  componentDidMount() {
    BooksAPI.getAll().then(books => {
      this.setState({
        books: books,
      });
    });
  }

  updateBook(book, shelf) {
    BooksAPI.update(book, shelf).then(() => {
      book.shelf = shelf;
      this.setState(prevState => ({
        books: prevState.books
          .filter(prevBook => book.id !== prevBook.id)
          .concat(book)
      }));
    });
  };

  render() {
    return (
      <div className="app">
        <Switch>
          <Route exact path='/' render={() => (
            <OverviewPage books={this.state.books} onUpdateBook={(book, shelf) => this.updateBook(book, shelf)}/>
          )}/>
          <Route path='/search' render={() => (
            <SearchPage books={this.state.books} onUpdateBook={(book, shelf) => this.updateBook(book, shelf)}/>
          )}/>
        </Switch>
      </div>
    )
  }
}

export default BooksApp